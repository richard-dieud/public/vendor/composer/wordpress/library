<?php

namespace Rd\Vendor\Wordpress\Utils;

class MetaUtils
{
    public static function filter_real_metas($wp_post_meta, $prefix)
    {
        $post_meta_data = [];
        foreach ($wp_post_meta as $post_meta_key => $post_meta_val) {
            if (strpos($post_meta_key, $prefix) !== false) {
                $post_meta_key = substr($post_meta_key, mb_strlen($prefix));
                $post_meta_data[$post_meta_key] = $post_meta_val && is_array($post_meta_val) ? current($post_meta_val) : "";
            }
        }

        return $post_meta_data;
    }
}

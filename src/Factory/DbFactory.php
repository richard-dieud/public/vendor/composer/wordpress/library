<?php

namespace Rd\Vendor\Wordpress\Factory;

use Rd\Vendor\Php\Model\Something;
use Rd\Vendor\Php\Utils\StrLib;

class DbFactory
{
    const __MAX_ROWS = 10000;

    public static function getTableName($instance, $add_prefix = false)
    {
        global $wpdb;

        $nextObject = self::getObject($instance);

        if (!$nextObject || empty($nextObject->getTableName())) {
            return;
        }

        $tableName =  $nextObject->getTableName();

        // Normalization
        $tableName = $add_prefix ? $wpdb->prefix . $nextObject->getTableName() : $tableName;

        return $tableName;
    }

    public static function getObject($instance)
    {
        $nextObject = null;
        if ($instance && is_string($instance) && class_exists($instance) && is_subclass_of($instance, Something::class)) {
            $nextObject = new $instance();
        } else if ($instance && is_object($instance) && class_exists(get_class($instance)) && is_subclass_of($instance, Something::class)) {
            $nextObject = $instance;
        } else {
            return;
        }

        return $nextObject;
    }

    public static function __callStatic($name, $args)
    {
        if (!empty($name) && is_string($name) && strpos($name, 'getBy') === 0 && strlen($name) > strlen('getBy') && is_array($args) && !empty($args)) {
            $IDType = explode('getBy', $name)[1];
            $args[] = $IDType;
            return forward_static_call_array([__CLASS__, 'getBy'], $args);
        }
    }

    public static function hydrate($instanceClass, $withData)
    {
        if (!in_array('strtosnake', get_class_methods(StrLib::class))) {
            throw new \Error('Call to an undefined function');
        }

        $results = [];
        $nextObject = self::getObject($instanceClass);
        if (is_object($nextObject) && is_array($withData) && !empty($withData)) {
            $instance = get_class($nextObject);
            foreach ($withData as $row) {
                $item = new $instance();
                foreach ($row as $k => $v) {
                    $methodName = StrLib::strtopascal('set_' . $k);
                    $item->$methodName($v);
                }
                $results[] = $item;
            }
        }

        return $results;
    }

    public static function normalizeProps($props)
    {
        if (!in_array('strtosnake', get_class_methods(StrLib::class))) {
            throw new \Error('Call to an undefined function');
        }

        $nextProps = [];
        if (is_array($props) && count($props)) {
            foreach ($props as $k => $v) {
                $nextProps[StrLib::strtosnake($k)] = $v === null || $v === [] ? 'null' : $v;
            }

            return $nextProps;
        }
    }

    /* ==========  ========== DATABASE OPERATIONSS ========== ========== */

    public static function createTable($tableName, $query)
    {
        global $wpdb;

        if ($wpdb->get_var("SHOW TABLES LIKE '$tableName'") != $tableName) {

            require_once ABSPATH . 'wp-admin/includes/upgrade.php';

            // dbDelta() permet de créer et mettre à jour une table dans la DB de manière simple
            // La fonction a une structure de controle qui permet d'ajouter la table si elle existe, sinon de l'update de la mnière suivante
            /*
                    - Les nouveaux champs sont ajoutés. 
                    - Les champs qui diffèrent dans leur type ou dans leur longueur sont ajustés. 
                    - Si le nom d'un champ est modifié, un nouveau champ sera créé avec le nouveau nom. L'ancien champ demeurera inchangé. 
                    - dbDelta() ne supprimera aucun champ d'une table existante.
                */
            dbDelta($query);
        }
    }

    public static function dropTable($tableName)
    {
        global $wpdb;

        if ($wpdb->get_var("SHOW TABLES LIKE '$tableName'") == $tableName) {
            $sql = "DROP TABLE $tableName";

            // Lance la requête directement au niveau de la base de données
            $wpdb->query($sql);
        }
    }

    public static function tableExists($tableName)
    {
        global $wpdb;
        return $wpdb->get_var("SHOW TABLES LIKE '$tableName'") == $tableName;
    }

    /* ========== ========= QUERY BUILDER ========== ========= */

    public static function buildInsertQuery($instance, $props, $issetOnly = false)
    {
        $tableName = self::getTableName($instance);

        $props = self::normalizeProps($props);
        $props = array_filter(
            $props,
            function ($v) use ($issetOnly) {
                if ($issetOnly && !empty($v) || !$issetOnly) {
                    return true;
                }
            }
        );
        $tableKeys = array_keys($props);

        $tableKeys = implode("`, `", $tableKeys);

        $sql = "INSERT INTO `$tableName` (`$tableKeys`) ";
        $sql .= " VALUES (";

        $arr = array_pad([], count($props), "%s");

        $sql .= implode(", ", $arr);
        $sql .= ") ";

        return $sql;
    }

    public static function buildCollectionInsertQuery($tableName, $tableKeys, $data, $issetOnly = false)
    {
        $tablevalues = [];
        foreach ($data as $row) {
            $props = self::normalizeProps($row);

            $props = array_filter(
                $props,
                function ($v) use ($issetOnly) {
                    if ($issetOnly && !empty($v) || !$issetOnly) {
                        return true;
                    }
                }
            );

            unset($props['id']);

            $rowValues = array_values($props);
            @$rowValues = '("' . implode('", "', $rowValues) . '")';

            $tablevalues[] = $rowValues;
        }

        $tableKeys = array_values($tableKeys);
        $tableKeys = "`" . implode("`, `", $tableKeys) . "`";

        $sql = "INSERT INTO `$tableName` ($tableKeys) VALUES ";
        $sql .= implode(",", $tablevalues) . ";";

        return $sql;
    }

    public static function buildUpdateQuery($ID, $newInstance)
    {
        $tableName = self::getTableName($newInstance);

        $props = $newInstance->getProps();
        $props = self::normalizeProps($props);

        $sql = "UPDATE `$tableName` SET ";

        $arr = [];
        foreach ($props as $propKey => $propVal) {
            $propVal = !empty($propVal) ? "'$propVal'" : 'NULL';
            $arr[] = "$propKey = $propVal";
        }
        $sql .= implode(", ", $arr);

        $sql .= " WHERE id = $ID";

        return $sql;
    }

    public static function buildSelectQuery($instanceClass, $ID = null, $where = null, $orderBy = "", $limit = 30)
    {
        $tableName = self::getTableName($instanceClass, true);

        $sql = "SELECT * FROM `$tableName`";
        if (!empty($ID) && !empty($where)) {
            $sql .= " WHERE $where = '$ID'";
        }

        if ($orderBy) {
            $sql .= " ORDER BY $orderBy";
        }

        if ($limit && intval($limit, 10) < self::__MAX_ROWS) {
            $sql .= " LIMIT $limit";
        }

        return $sql;
    }

    public static function buildDeleteQuery($instanceClass, $limit = null)
    {
        $tableName = self::getTableName($instanceClass);

        $sql = "DELETE FROM `$tableName` WHERE id = %s";
        $sql .= " LIMIT $limit";


        return $sql;
    }

    /* ==========  ========== TABLE OPERATIONSS ========== ========== */

    public static function add($instance, $issetOnly = false)
    {
        global $wpdb;

        if (isset($instance) && $instance instanceof Something) {
            $props = $instance->getProps();

            $props = array_filter(
                $props,
                function ($v) use ($issetOnly) {
                    if ($issetOnly && !empty($v) || !$issetOnly) {
                        return true;
                    }
                }
            );

            $props['created_at'] = (new \DateTime('', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s');

            $sql = self::buildInsertQuery($instance, $props, $issetOnly);

            $sql = $wpdb->prepare($sql, $props);
            $result = $wpdb->query($sql);

            return $result ? $wpdb->insert_id : false;
        }
    }

    public static function addCollection(array $data, $instance, $issetOnly = false)
    {
        global $wpdb;

        $nextCollection = [];
        if (!empty($data) && $instance && $nextObject = self::getObject($instance)) {
            foreach ($data as $row) {
                $props = $row->getProps();

                $props = array_filter(
                    $props,
                    function ($v) use ($issetOnly) {
                        if ($issetOnly && !empty($v) || !$issetOnly) {
                            return true;
                        }
                    }
                );

                // unset($props["ID"]);
                $props['created_at'] = (new \DateTime('', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s');

                $nextCollection[] = $props;
            }

            $tableName = self::getTableName($nextObject, true);

            $tableKeys = $nextObject->getProps(true);
            $tableKeys = array_keys($tableKeys);

            // dump($tableKeys, $nextCollection[0]);
            // die();

            $sql = self::buildCollectionInsertQuery($tableName, $tableKeys, $nextCollection, $issetOnly);

            $result = $wpdb->query($sql);

            return $result;
        }
    }

    public static function update($ID, $newInstance)
    {
        global $wpdb;

        if (isset($newInstance) && $newInstance instanceof Something && null !== self::get(get_class($newInstance), $ID) && !empty($newInstance->getID())) {
            $newInstance->setUpdatedAt((new \DateTime('', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s'));
            $sql = self::buildUpdateQuery($ID, $newInstance);

            // dump($sql);

            // @todo IMPORTANT Use PREPARE for security reasons
            // $sql = $wpdb->prepare($sql, $props);
            $result = $wpdb->query($sql);

            return $result;
        }
    }

    public static function remove($ID, $instanceClass, $limit = null)
    {
        global $wpdb;

        if (!empty($ID) && isset($instanceClass) && null !== self::get($instanceClass, $ID)) {
            $sql = self::buildDeleteQuery($instanceClass, $limit);

            $sql = $wpdb->prepare($sql, $ID);
            $result = $wpdb->query($sql);

            return $result;
        }
    }

    public static function get($instanceClass, $ID)
    {
        $data = self::getBy($instanceClass, $ID, 'id', 1);
        return !empty($data) && is_array($data) && count($data) === 1 ? current($data) : $data;
    }

    public static function getBy($instanceClass, $ID, $where, $orderBy = "", $limit = "")
    {
        global $wpdb;

        $data = null;

        if ($instanceClass && is_string($instanceClass) && class_exists($instanceClass) && !empty($ID) && !empty($where)) {
            $where = trim(strtolower($where));
            $instance = new $instanceClass();
            if (!isset($instance) || !($instance instanceof Something)) {
                throw new \Error('Unknown object, identified by: ' . $instance);
            }

            $props = $instance->getProps();
            $propKeys = array_map('strtolower', array_keys($props));
            $props = array_combine($propKeys, array_values($props));

            if (!array_key_exists($where, $props)) {
                throw new \Error('GetBy can not be processed with this ID type');
            }

            $sql = self::buildSelectQuery($instanceClass, $ID, $where, $orderBy, $limit);

            if ($sql) {
                $data = $wpdb->get_results($sql);

                // Hydrate with results
                $data = self::hydrate($instanceClass, $data);
            }

            return $data;
        }
    }

    public static function getList($instanceClass, $limit = null)
    {
        global $wpdb;

        $data = null;

        if ($instanceClass) {
            $sql = self::buildSelectQuery($instanceClass, null, null, "name asc", $limit);

            if ($sql) {
                $data = $wpdb->get_results($sql);

                // Hydrate with results
                $data = self::hydrate($instanceClass, $data);
            }
        }

        return $data;
    }
}
